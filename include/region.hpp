#ifndef DENOISE_REGION__HPP
#define DENOISE_REGION__HPP

#include <algorithm>
#include <boost/interprocess/smart_ptr/unique_ptr.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/utility.hpp>
#include <cstdlib>
#include <deque>
#include <Eigen/Dense>
#include <iostream>
#include <map>
#include <set>
#include <stdexcept>
#include <vector>

#include <sample.hpp>

namespace denoise {

typedef std::set<int64_t> pixel_set;

// A region is a 4-connected subset of the image. Each image location is
// represented via a pixel index.
class region
{
	public:

	typedef Eigen::Matrix<float, 6, 1> model_type;

	region() { clear(); }
	region(int64_t idx, const sample_image& im) { set_pixel(idx, im); }

	// Reset the region to an empty state
	void clear();

	// Is this region empty?
	bool empty() const { return pixels_.empty(); }

	const pixel_set& pixels() const { return pixels_; }
	const pixel_set& neighbours() const { return neighbours_; }

	// evaluate the model at a specified pixel position and return the value.
	sample model_at(float x, float y, const sample_image& samples) const;

	// In-place merge this region with r assuming both regions come from
	// the specified image.
	void merge(const region& r, const sample_image& im);

	// Assign this region the single pixel at index idx within im.
	void set_pixel(int64_t idx, const sample_image& im);

	float log_lik() const { return log_likelihood_; }

	float log_prior() const { return log_prior_; }

	int64_t size() const { return n_samples_; }

	int64_t allowed_size() const { return n_allowed_samples_; }

	const sample& mean() const { return mean_; }

	const sample& variance() const { return variance_; }

	const model_type& model() const { return model_; }

	protected:

	// A set of pixels making up the region. Call update_model if this is
	// changed.
	pixel_set pixels_;

	// The neighbouring pixels for this region
	pixel_set neighbours_;

	// The plane which makes up this region
	model_type model_;

	// A cache of the number of samples we have
	int64_t n_samples_;

	// A cache of the number of non-zero samples we have
	int64_t n_non_zero_samples_;

	// A cache of the number of samples in the 'allowed region'
	int64_t n_allowed_samples_;

	// mean of the samples in the region
	sample mean_;

	// variance of the samples in the region
	sample variance_;

	// The C-vM based log likelihood for this region.
	float log_likelihood_;

	// The prior on this region based on sample size and model.
	float log_prior_;

	// Update the region's plane assuming the region's pixels come from the
	// specified image.
	void update_model(const sample_image& im);

	// Update the region's p-value and prior.
	void update_probs(const sample_image& im);

	void update_all(const sample_image& im)
		{ update_model(im); update_probs(im); }

	public:

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

namespace internal {

// A cache of index pairs where one may efficiently:
//
//  - add a pair of indices to the cache
//  - query if a pair of indices are in the cache
//  - remove all pairs containing an index from the cache
//  - reassign all references to idx1 to idx2
//
// The cache is implemented as a map from index to a list of indices which form
// a pair with it.
class index_pair_cache
{
	public:

	typedef std::pair<int64_t, int64_t> index_pair_type;

	void insert(const index_pair_type& p);
	bool contains(const index_pair_type& p) const;
	void erase(const index_pair_type& p);
	void erase_index(int64_t idx);
	void clear();
	void reassign(int64_t from_idx, int64_t to_idx);
	int64_t size() const;

	protected:

	typedef std::set<int64_t> index_set_type;
	typedef std::map<int64_t, index_set_type> index_map_type;

	index_map_type index_map_;
};

}

// An image is made up of a set of regions
class region_image : protected boost::noncopyable
{
	public:

	typedef boost::shared_ptr<sample_image> sample_image_ptr;
	typedef boost::shared_ptr<region> region_ptr;
	typedef std::deque<region_ptr> region_collection_t;

	region_image() : p_sample_image_(new sample_image()) { reset_regions(); }
	region_image(const sample_image_ptr& ps) { set_sample_image(ps); }

	void set_sample_image(const sample_image_ptr& ps); 
	const sample_image_ptr& ptr_to_sample_image() const { return p_sample_image_; }

	int64_t size() const { return regions_.size(); }

	// Return a reference to the region for a given pixel (const and bounds checked)
	const region& region_for_index(int64_t ri) const {
		if((ri < 0) || (ri >= size()))
			throw std::runtime_error("invalid region index");
		return *(regions_[ri]);
	}

	// Return a reference to the region for a given pixel (const and bounds checked)
	const region& region_for_pixel(int64_t pi) const { return region_for_index(region_index_for_pixel(pi)); }

	// Write a set of region indices corresponding to the neighbours of the specified region.
	void region_neighbours(const region& r, std::set<int64_t>& out_indices) const;

	// Convenience version of region_neighbours for an output iterator.
	template<typename OutputIterator>
	void region_neighbours(const region& r, OutputIterator out) const
		{ std::set<int64_t> oi; region_neighbours(r, oi); std::copy(oi.begin(), oi.end(), out); }

	// Return an index for the given region. This is mostly just useful for
	// colouring and for passing to merge regions.
	int64_t region_index_for_pixel(int64_t pi) const {
		if((pi < 0) || (static_cast<size_t>(pi) >= pixel_regions_.size()))
			throw std::runtime_error("invalid pixel index");
		return pixel_regions_[pi];
	}

	// Attempt to merge some neighbour of the region according to the merge
	// rule. If no neighbour could be merged, return false otherwise return
	// true. All neighbours are tested and the first mergable one is
	// merged. Neighbours are tried in a random order.
	bool merge_region_neighbour(int64_t r_idx);

	// merge two regions and return the new region's index
	int64_t merge_regions(int64_t r1_idx, int64_t r2_idx);

	// convenience member
	std::istream& read_samples(std::istream& is);

	// write a brief description of the region image metrics to the output
	// stream.
	std::ostream& write_metrics(std::ostream& os);

	protected:

	// The underlying sample image.
	sample_image_ptr p_sample_image_;

	// The regions in the current image. Not all will be non-empty.
	region_collection_t regions_;

	// Effectively a map between pixel index and an index into regions for
	// the region that contains it.
	std::vector<int64_t> pixel_regions_;

	// Return a reference to the region for a given pixel (non-const)
	region& region_for_pixel(int64_t pi) { return *(regions_[pixel_regions_[pi]]); }

	// Return a reference to the region for a given pixel (non-const)
	region& region_for_index(int64_t ri) { return *(regions_[ri]); }

	// A set of 'neighbour stable' regions. A region is neighbour stable if
	// last time we looked at it, none of it's neighbours could be merged
	// with it *and* none of it's neighbours have been merged in the
	// interrim.
	std::set<int64_t> neighbour_stable_set_;

	// a cache of region index pairs which have been onserved *not* to be mergable
	internal::index_pair_cache index_pair_cache_;

	// Replace regions r1 and r2 with the region pointed to by pr. Return
	// the index of the new region.
	// 
	// IT IS VITAL THAT THE NEW REGION BE THE MERGER OF R1 AND R2.
	int64_t replace_regions(int64_t r1_idx, int64_t r2_idx, region_ptr pr);

	// return a flag indicating whether r1 and r1 should be merged into merged.
	bool should_merge(const region& r1, const region& r2, const region& merged);

	void reset_regions();
};

// convenience functions

// Write a map of the regions to the named PNG file. The regions are coloured
// pseudo-randomly.
//
// Returns negative on failure, zero on success.
int write_region_map_png(const region_image&, const char* filename);

// Write an image of the region models to a PNG file (this is generally the
// denoised image).
//
// Returns negative on failure, zero on success.
int write_region_model_png(const region_image&, const char* filename);

}

#endif // DENOISE_REGION__HPP
