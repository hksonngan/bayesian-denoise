#ifndef DENOISE_UTIL__HPP
#define DENOISE_UTIL__HPP

#include <algorithm>
#include <stdexcept>
#include <stdint.h>

namespace denoise { namespace internal {

/// @brief Write a packed array of pixels to a PNG.
///
/// @note This is mostly lifted from http://www.lemoda.net/c/write-png/
///
/// @param pixela An array of 3*width + height bytes representing the packed RGB pixels to write.
/// @param w The width of the the image in pixels.
/// @param h The height of the image in pixels.
/// @param path
///
/// @return Negative on failure, zero on success.
int write_rgb_bitmap_to_png(const uint8_t* pixels, int64_t w, int64_t h, const char *path);

/// Given a Cramer-von Mises statistic and the size of the two sample
/// sequences, compute a p-value.
///
/// The p-value *decreases* as the likelihood of the null hypothesis *increases*.
float cm_pvalue(float cms, int64_t N1, int64_t N2);

/// Calculate the Cramer-von Mises two sample statistic.
///
/// @sa http://en.wikipedia.org/wiki/Cramer-von-Mises_criterion
///
/// @note [first1, last1) and [first2, last2) are assume to be sorted in ascending order.
template<typename InputIterator>
float cm_statistic_when_sorted(
		InputIterator first1, InputIterator last1,
		InputIterator first2, InputIterator last2)
{
	int64_t N(0), M(0);
	double U(0.f), U1(0.f), U2(0.f);

	for(int64_t current_rank=0; (first1 != last1) || (first2 != last2); ++current_rank)
	{
		// which is the next sample in the sequence?
		if((first2 == last2) || ((first1 != last1) && (*first1 < *first2)))
		{
			// sequence 1 is next
			int64_t delta(current_rank - N);
			U1 += delta * delta;
			++N; ++first1;
		}
		else
		{
			// sequence 2 is next
			int64_t delta(current_rank - M);
			U2 += delta * delta;
			++M; ++first2;
		}
	}

	// do a sanity check
	if((first1 != last1) || (first2 != last2))
	{
		throw std::runtime_error("Error calculating C-vM statistic. Was input sorted?");
	}

	U = N*U1 + M*U2;

	return U/static_cast<float>(N*M*(N+M)) - (4.f*M*N - 1.f)/(6.f*(M+N));
}

// like the above, but sorts sequence 1 and 2 first
template<typename RandomAccessIterator>
float cm_statistic(
		RandomAccessIterator first1, RandomAccessIterator last1,
		RandomAccessIterator first2, RandomAccessIterator last2)
{
	std::sort(first1, last1);
	std::sort(first2, last2);
	return cm_statistic_when_sorted(first1, last1, first2, last2);
}

} }

#endif // DENOISE_UTIL__HPP
