#!/bin/sh

cd cat

function delta() {
	convert $1 $2 -fx '0.5*(u-v) + 0.5' -type Grayscale -normalize $3
}

prefix=teaser-crop

delta original.png model.png teaser-delta.png
for i in model original map teaser-delta; do
	convert $i.png -crop 128x128+164+82 ${prefix}-$i.png
done
montage ${prefix}-original.png ${prefix}-map.png ${prefix}-teaser-delta.png ${prefix}-model.png -tile x1 -geometry +1+1 teaser-montage.png
