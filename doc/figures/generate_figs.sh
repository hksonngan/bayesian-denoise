#!/bin/sh

function delta() {
	#convert $1 $2 -fx '30*0.5*(u-v) + 0.5' -type Grayscale $3
	convert $1 $2 -fx '0.5*(u-v) + 0.5' -type Grayscale -normalize $3
}

function denoise() {
	dir=$1
	bl_thresh=$2
	if [ -z $dir ]; then
		echo "Must specify source directory."
		exit 1
	fi

	pushd $dir
	convert original.png -selective-blur 0x5+${bl_thresh}% bilateral.png
	delta original.png model.png bayes-delta.png
	delta original.png bilateral.png bilateral-delta.png
	delta ground-truth.png model.png bayes-gt-delta.png
	delta ground-truth.png bilateral.png bilateral-gt-delta.png
	delta original.png ground-truth.png gt-delta.png
        #convert \
	#	\( original.png -scale 400% \) \
	#	\( map.png -colorspace gray -scale 400% -edge 0.25 -white-threshold 0.01 -negate \) \
	#	-compose Multiply -composite map2.png
	popd
}

function process() {
	dir=$1
	x=$2
	y=$3

	pushd $dir
	for i in bilateral original model; do
		convert $i.png -crop 64x64+$x+$y crop-$i.png
	done
	montage crop-bilateral.png crop-original.png crop-model.png -geometry +2+2 montage.png
	popd
}

denoise cat 4
denoise classroom 2

#process cat 204 180 5
process cat 190 100
#process classroom 77 118
process classroom 130 50

