#include <algorithm>
#include <boost/foreach.hpp>
#include <boost/shared_ptr.hpp>
#include <cstdlib>
#include <ctime>
#include <deque>
#include <error.h>
#include <fstream>
#include <error.h>

#include <region.hpp>
#include <sample.hpp>
#include <util.hpp>

using namespace denoise;

int main(int argc, char **argv)
{
	if(argc < 2)
		error(EXIT_FAILURE, 0, "need input file");

	std::cout << "Reading samples from " << argv[1] << std::endl;
	std::ifstream is(argv[1]);

	region_image region_im;
	region_im.read_samples(is);

	std::cout << "Read " << region_im.ptr_to_sample_image()->size() << " samples.\n";

	std::cout << "Denoising..." << std::endl;
	write_region_map_png(region_im, "map.png");
	write_region_model_png(region_im, "model.png");
	write_region_model_png(region_im, "original.png");

	int64_t n_left = 0; // (w*h)*0.5f;
	clock_t update_clock = clock(), png_clock = clock();
	int64_t n_merges_last_pass = 0;
	do
	{
		// create a vector of region indices to merge
		std::vector<int64_t> region_indices(region_im.size());
		for(size_t i=0; i<region_indices.size(); ++i)
			region_indices[i] = i;
		std::random_shuffle(region_indices.begin(), region_indices.end());

		n_merges_last_pass = 0;
		BOOST_FOREACH(int64_t r1_idx, region_indices)
		{
			// we may have already merged a region at this index
			if(r1_idx >= region_im.size())
				continue;

			const region& r1(const_cast<const region_image&>(region_im).region_for_index(r1_idx));
			if(r1.empty())
				throw std::runtime_error("Unexpected empty region.");

			// try to merge a neighbour
			if(region_im.merge_region_neighbour(r1_idx))
				++n_merges_last_pass;

			if(clock() >= update_clock)
			{
				region_im.write_metrics(std::cout) << std::endl;
				update_clock += 2 * CLOCKS_PER_SEC;
			}

			if(clock() >= png_clock)
			{
				write_region_map_png(region_im, "map.png");
				write_region_model_png(region_im, "model.png");
				png_clock += 13 * CLOCKS_PER_SEC;
			}
		}
	}
	while((n_merges_last_pass > 0) && (region_im.size() >= n_left));

	region_im.write_metrics(std::cout) << std::endl;
	write_region_map_png(region_im, "map.png");
	write_region_model_png(region_im, "model.png");
	std::cout << '\n';

	return EXIT_SUCCESS;
}
